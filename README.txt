CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Usage
 * Maintainers


INTRODUCTION
------------

This module gives the flexibility to retain query parameters during
user's page visits. Also it provides the validation check based on
predefined parameter set configured from the back-end to validate
desired URL's. It can be used by users to pass parameters throughout the site.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information. Example :

    1. Copy persistent_url_parameters folder to modules directory
      (sites/all/modules).
    2. At admin/modules enable the Persistent URL Parameters module.


CONFIGURATION
-------------

The configuration page for this module is at:
Configuration > URL parameter settings > Url parameter
(admin/config/campaign-parameters/global)

 * URL parameter links format:
   Value in Url parameter textbox can list the mandatory parameters for
   validation separated by "|".


USAGE
-----

Campaigns which generate URL's for different social media sites.
For example facebook, twitter etc.

When user clicks on one of the campaign URL shared on facebook/twitter it lands
on site with the query parameters.

When that user roams around on your site you would like to have that query
parameters to be persistent on the URL's throughout his/her page visits.

This module would give you the flexibility to retain query parameters during
user's page visits. Also it will provide the validation check based on
predefined parameter set configured from the back-end to validate desired URL's.


MAINTAINERS
-----------

 * Soumya Das - http://drupal.org/u/soumyadas
