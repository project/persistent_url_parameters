/**
 * @file
 * Implements javascript to retain url parameters based on conditions.
 */
(function ($) {

  'use strict';

  Drupal.behaviors.persistent_url_parameters = {
    attach: function (context, settings) {
      var querystring = window.location.href.slice(window.location.href.indexOf('?') + 1);
      querystring = decodeURI(querystring);
      var list = Drupal.settings.persistent_url_parameters.list;
      list = list.split('|');
      var is_present = false;
      list.forEach(function (entry) {
        if (querystring.toLowerCase().match(entry + '=')) {
          is_present = true;
        }
      });

      if (is_present) {

        // Store parameters in cookie coming directly from campaign urls
        // to avoid additional parameters on a subsequent page request.
        var parameters_in_cookie = $.cookie('persistent_url_parameters');

        if(typeof parameters_in_cookie === 'undefined'){
          $.cookie('persistent_url_parameters', querystring, {
            path: '/',
            expires: 1
          });
        }
        else if(typeof parameters_in_cookie !== 'undefined' && !(querystring.match(parameters_in_cookie) && querystring.length === parameters_in_cookie.length)) {
          $.cookie('persistent_url_parameters', querystring, {
            path: '/',
            expires: 1
          });
        }
        else {
          querystring = parameters_in_cookie;
        }

        $('a').each(function (event) {
          // If anchor tag already processed.
          if ($(this).hasClass('link-string-processed')) {
            return false;
          }

          var href = $(this).attr('href');
          var target = $(this).attr('target');

          // Check for invalid urls. Replaced javascript:void(0) to  event.preventDefault()
          if (href && (!target || target !== '_blank') && (href !== "javascript:void(0)") && (href !== '#')) {

            // Avoid anchor tag starting with hash
            if (typeof href !== 'undefined' && href.match('^#') !== 'undefined' && href.match('^#') === null) {

              // If anchor tag contains parameters.
              if (href.match(/\?/)) {

                // Check for parameterized urls.
                var anchor_params = href.slice(href.indexOf('?') + 1);
                var anchor_path = href.slice(0, href.indexOf('?'));

                // Reconstruct url based on hash found on anchor tag.
                if (anchor_params.match('#')) {
                  var anchor_params_without_hash = anchor_params.slice(0, anchor_params.indexOf('#'));
                  var anchor_hash = anchor_params.slice(anchor_params.indexOf('#'));

                  // If anchor parameters not matches with query parameters.
                  if (!anchor_params_without_hash.match(querystring)) {
                    var overridden_params = Drupal.behaviors.persistent_url_parameters.remove_overridden_link_params(anchor_params_without_hash, querystring);
                    overridden_params = typeof overridden_params == 'undefined' ? '' : overridden_params + '&';
                    href = anchor_path + '?' + overridden_params + querystring + anchor_hash;
                  }
                }
                else {
                  // If anchor parameters not matches with query parameters.
                  if (!anchor_params.match(querystring)) {
                    var overridden_params = Drupal.behaviors.persistent_url_parameters.remove_overridden_link_params(anchor_params, querystring);
                    overridden_params = typeof overridden_params == 'undefined' ? '' : overridden_params + '&';
                    href = anchor_path + '?' + overridden_params + querystring
                  }
                }
              }
              else {
                // Anchor tag doesn't contains parameters so add querystring 
                // except 'admin' pages.
                if(!href.match(/admin/g)) {
                    href += '?' + querystring;
                }                
              }
              $(this).attr('href', encodeURI(href));
              $(this).addClass('link-string-processed');
            }
          }
        });
      }
    },
    remove_overridden_link_params: function(link_params, query_string_params) {
      // If any parameter from query_string_params matched with link_params 
      // then it will be removed from link_params as favoured of 
      // query_string_params. It means parameters with the same name can 
      // exist once.

      var query_key_array = [];
      var query_string_params_array = query_string_params.split('&'); 

      for(var i=0; i<query_string_params_array.length; i++) {
        query_key_array.push(query_string_params_array[i].split('=')[0]);
      }

      var link_params_array = link_params.split('&');

      for(var i=0; i<link_params_array.length; i++) {            
        var link_param = link_params_array[i].split('=')[0];
        for(var j=0; j<query_key_array.length; j++) {
          if(link_param == query_key_array[j]) {
            link_params_array.splice(i,1);
            i--;
          }
        }
      }
    
      return link_params_array.join("&");
    }
  };  
})(jQuery);

